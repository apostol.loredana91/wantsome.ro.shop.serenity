package API;

import com.google.gson.JsonParser;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.restassured.RestAssured.given;

@RunWith(DataProviderRunner.class)
public class VerifyingResponseBody {
    @UseDataProvider("data")
    @Test
    public void testAPi(String requestFileName, String responseFileName) throws IOException {
        String requestBody = readFile(requestFileName, StandardCharsets.UTF_8);
        String responseBody = readFile(responseFileName, StandardCharsets.UTF_8);
        JsonParser jsonParser = new JsonParser();
        String inlineJsonResponse = jsonParser.parse(responseBody).toString();

        String body = given().contentType(ContentType.JSON).body(requestBody)
                .when().post("http://petstore.swagger.io/v2/pet")
                .getBody().asString();
        Assert.assertEquals(inlineJsonResponse, body);
    }

    @DataProvider
    public static Object[][] data() {
        return new Object[][]{
                {"E:\\Wantsome-Shop-Serenity\\src\\test\\resources\\API_testing_resources\\requestToCreateGrivei.json",
                        "E:\\Wantsome-Shop-Serenity\\src\\test\\resources\\API_testing_resources\\responseOfGriveiCreation.json"},
                {"E:\\Wantsome-Shop-Serenity\\src\\test\\resources\\API_testing_resources\\requestToCreatePatrocle.json",
                        "E:\\Wantsome-Shop-Serenity\\src\\test\\resources\\API_testing_resources\\responseOfPatrocleCreation.json"}
        };
    }

    static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
