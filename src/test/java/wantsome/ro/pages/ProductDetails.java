package wantsome.ro.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDetails extends PageObject {
    private WebDriver driver;

    @FindBy(css = ".product_title")
    @CacheLookup
    WebElement productTitle;

    @FindBy(css = ".zoomImg")
    @CacheLookup
    WebElement productImage;

    @FindBy(css = ".posted_in")
    @CacheLookup
    WebElement productCategory;

    @FindBy(id = "tab-title-description")
    @CacheLookup
    WebElement productDetailsTab;

    @FindBy(css = "[role='tabpanel']:nth-child(2) p ")
    @CacheLookup
    WebElement productDescriptionText;

    @FindBy(id = "tab-title-reviews")
    @CacheLookup
    WebElement productReviewTab;

    @FindBy(className = "star-5 active")
    WebElement fiveStars;

    @FindBy(id = "comment")
    WebElement reviewDescription;

    @FindBy(id = "author")
    WebElement reviewAuthor;

    @FindBy(id = "email")
    WebElement reviewEmail;

    @FindBy(id = "submit")
    WebElement submitRewiewButton;

    @FindBy(css = "ol.commentlist > :last-child > div > div >div.description >p")
    WebElement lastReviewDescription;

    @FindBy(xpath = "//input[@name='quantity']")
    @CacheLookup
    WebElement productQuantity;

    @FindBy(css = ".single_add_to_cart_button")
    @CacheLookup
    WebElement addToCartButton;

    @FindBy(css = "[id='woocommerce_widget_cart-1'] .buttons .wc-forward:nth-of-type(1)")
    @CacheLookup
    WebElement viewCart;

    @FindBy(css = "[id='woocommerce_widget_cart-1'] .checkout")
    @CacheLookup
    WebElement checkout;

    @FindBy(xpath = "//div[@class='woocommerce-message']")
    WebElement confirmationMessage;

    public ProductDetails(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getProductTitle() {
        return productTitle.getText();
    }

    public void addToCart(String quantity) {
        productQuantity.clear();
        productQuantity.sendKeys(quantity);
        addToCartButton.click();
    }

    public String getProductAddedToCartMessage() {
        return confirmationMessage.getText();
    }

    public String getProductQuantity() {
        return productQuantity.getAttribute("value");
    }

    public void addReview(String description, String name, String email) {
        productReviewTab.click();
        fiveStars.click();
        reviewDescription.sendKeys(description);
        reviewAuthor.sendKeys(name);
        reviewEmail.sendKeys(email);
        submitRewiewButton.click();
    }

    public String getLastReviewDescription() {
        return lastReviewDescription.getText();
    }

    public void navigateToCart() {
        viewCart.click();
    }
}
