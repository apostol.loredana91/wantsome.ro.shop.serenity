package wantsome.ro.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccount extends PageObject {

    private WebDriver driver;

    //Login Webelements
    @FindBy(id = "username")
    WebElement username;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(css = "[name='login']")
    WebElement loginButton;

    @FindBy(id = "rememberme")
    WebElement rememberMeCheckBox;

    @FindBy(css = ".woocommerce-error li")
    @CacheLookup
    WebElement fieldsEmptyError;

    @FindBy(css = ".woocommerce-error li")
    WebElement passFieldEmptyError;

    @FindBy(css = ".woocommerce-error li")
    WebElement unregisteredUserError;

    //Register Webelements
    @FindBy(id = "reg_email")
    @CacheLookup
    WebElement registrationEmail;

    @FindBy(id = "reg_password")
    @CacheLookup
    WebElement registrationPass;

    @FindBy(id = "registration_field_1")
    @CacheLookup
    WebElement firstName;

    @FindBy(id = "registration_field_2")
    @CacheLookup
    WebElement lastName;

    @FindBy(id = "registration_field_3")
    @CacheLookup
    WebElement phoneNo;

    @FindBy(css = "[name='register']")
    @CacheLookup
    WebElement registerButton;

    @FindBy(css = ".woocommerce-error li")
    @CacheLookup
    WebElement emptyRgistrationEmailFied;

    //My account Webelements
    @FindBy(linkText = "Dashboard")
    @CacheLookup
    WebElement dashboard;

    @FindBy(linkText = "Logout")
    WebElement logout;

    public MyAccount(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void verifyLoginPageTitle() {
        driver.getTitle();
    }

    public void authenticate(String user, String pass) {
        HomePage homePage = new HomePage(driver);
        homePage.navigateToRegisterOrLoginPage();
        username.sendKeys(user);
        password.sendKeys(pass);
        if (!rememberMeCheckBox.isSelected())
            rememberMeCheckBox.click();
        loginButton.click();
    }

    public String getFieldsEmptyError() {
        return fieldsEmptyError.getText();
    }

    public String getPassEmptyFieldError() {
        return passFieldEmptyError.getText();
    }

    public String getUnregisteredUserError() {
        return unregisteredUserError.getText();
    }

    public String getDashboardText() {
        return dashboard.getText();
    }

    public void registerAccount(String email, String pass, String fName, String lName, String phone) {
        HomePage homePage = new HomePage(driver);
        homePage.navigateToRegisterOrLoginPage();
        registrationEmail.sendKeys(email);
        registrationPass.sendKeys(pass);
        firstName.sendKeys(fName);
        lastName.sendKeys(lName);
        phoneNo.sendKeys(phone);
        registerButton.click();
    }

    public String getEmptyRegistrationEmailFieldError() {
        return emptyRgistrationEmailFied.getText();
    }

    public void logout() {
        logout.click();
    }

    //--------------------------------------------------------------------------
    public void setEmail(String email) {
        registrationEmail.sendKeys(email);
    }

    public void setPassword(String pass) {
        registrationPass.sendKeys(pass);
    }

    public void setFname(String fname) {
        firstName.sendKeys(fname);
    }

    public void setLastName(String lname) {
        lastName.sendKeys(lname);
    }

    public void setPhone(String phone) {
        phoneNo.sendKeys(phone);
    }

    public void clickSubmit() {
        registerButton.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
