package wantsome.ro.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductList extends PageObject {
    private WebDriver driver;
    @FindBy(css = ".orderby")
    WebElement sortByDropdown;

    @FindBy(css = ".orderby > option")
    List<WebElement> sortByOptions;

    @FindBy(css = ".products > li >h3 > a")
    List<WebElement> products;

    @FindBy(xpath = "//span[@class='price']/*[not(self::del)]")
    List<WebElement> productsPrices;

    public ProductList(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void sortBy(String userOption) {
        sortByDropdown.click();
        for (WebElement option : sortByOptions) {
            if (option.getText().equals(userOption)) {
                option.click();
                break;
            }
        }
    }

    public void clickOnProduct(Integer productNo) {
        products.get(productNo)
                .click();

    }

    public String getProductTitle(Integer productNo) {
        return
                products.get(productNo)
                        .getText();

    }

    public List<WebElement> getProductsPrices() {
        return productsPrices;
    }

    public List<Double> listOfPricesDigitsOnly(List<WebElement> listOfPrices) {
        List<Double> doublePrices = new ArrayList<>();
        for (WebElement price : listOfPrices) {
            String onlyDigitsPrice = price.getText();
            onlyDigitsPrice = onlyDigitsPrice.replaceAll("[^-,0-9]", "");
            onlyDigitsPrice = onlyDigitsPrice.replaceAll("[,]", ".");
            double doublePrice = Double.valueOf(onlyDigitsPrice);
            doublePrices.add(doublePrice);
        }
        return doublePrices;
    }

    public List<Double> orderByLowerPrice(List<Double> unorderedPrices) {
        Collections.sort(unorderedPrices);
        return unorderedPrices;
    }
}
