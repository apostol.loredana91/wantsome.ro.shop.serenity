package wantsome.ro;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import wantsome.ro.steps.*;

public class BaseTestClass {

    @Managed()
    public WebDriver driver;
    @Steps
    protected BrowserSteps browserSteps;
    @Steps
    protected HomeSteps homeSteps;
    @Steps
    protected RegistrationSteps registrationSteps;
    @Steps
    protected LoginSteps loginSteps;
    @Steps
    protected ProductListSteps productListSteps;
    @Steps
    protected ProductDetailsSteps productDetailsSteps;
    @Steps
    protected CartSteps cartSteps;

    @Before
    public void setupTest() {
        browserSteps.goToWantsomeShopPage("http://practica.wantsome.ro/shop/");
    }
}
