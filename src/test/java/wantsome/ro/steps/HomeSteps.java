package wantsome.ro.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import wantsome.ro.pages.HomePage;

public class HomeSteps extends ScenarioSteps {

    private HomePage homePO;

    @Step("Verify homepage title")
    public String getHomepageTitle() {
        return homePO.verifyPageHomePageTitle();
    }

    @Step("Navigate to Connect page")
    public void navigateToConnect() {
        homePO.navigateToRegisterOrLoginPage();
    }

    @Step("Navigate to Men Collection page")
    public void navigateToMenCollection() {
        homePO.navigateToMenCollection();
    }

    @Step("Navigate to Women Collection page")
    public void navigateToWomenCollection() {
        homePO.navigateToWomenCollection();
    }
}
