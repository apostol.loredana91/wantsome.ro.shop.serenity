package wantsome.ro.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.WebElement;
import wantsome.ro.pages.ProductList;

import java.util.List;

public class ProductListSteps extends ScenarioSteps {

    private ProductList productList;

    @Step("Sort products by selected category")
    public void sortBy(String userOption) {
        productList.sortBy(userOption);
    }

    @Step("Open a product page")
    public void clickOnProduct(Integer productNo) {
        productList.clickOnProduct(productNo);
    }

    @Step("Check product title")
    public String getProductTitle(Integer productNo) {
        return productList.getProductTitle(productNo);
    }

    @Step("Get unprocessed product prices list")
    public List<WebElement> getProductPrices() {
        return productList.getProductsPrices();
    }

    @Step("Get the list of product actual prices")
    public List<Double> getTheListOfActualProductPrices(List<WebElement> listOfPrices) {
        List<Double> actualProductPrices = productList.listOfPricesDigitsOnly(listOfPrices);
        return actualProductPrices;
    }

    @Step("Order the list of prices in order to obtain the expected list of prices")
    public List<Double> getTheListOfExpectedProductPrices() {
        List<Double> expectedProductPrices = productList
                .orderByLowerPrice(productList
                        .listOfPricesDigitsOnly(productList
                                .getProductsPrices()));
        return expectedProductPrices;
    }
}
