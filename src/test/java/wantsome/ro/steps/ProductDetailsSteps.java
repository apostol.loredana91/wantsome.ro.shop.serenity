package wantsome.ro.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import wantsome.ro.pages.ProductDetails;

public class ProductDetailsSteps extends ScenarioSteps {

    private ProductDetails productDetails;

    @Step("Check product title")
    public String getProductTitle() {
        return productDetails.getProductTitle();
    }

    @Step("Add a number of products to cart")
    public void addProductToCart(String quantity) {
        productDetails.addToCart(quantity);
    }

    @Step("Check if product was added to cart")
    public String productAddedToCartConfirmationMessage() {
        return productDetails.getProductAddedToCartMessage();
    }

    @Step("Find out the quantity of product added to cart")
    public String getProductQuantity() {
        return productDetails.getProductQuantity();
    }

    @Step("Add a product review")
    public void addReview(String description, String name, String email) {
        productDetails.addReview(description, name, email);
    }

    @Step("Check the last added review description")
    public String getLastReviewDescription() {
        return productDetails.getLastReviewDescription();
    }

    @Step("View cart")
    public void navigateToCart() {
        productDetails.navigateToCart();
    }
}
