package wantsome.ro.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import wantsome.ro.pages.MyAccount;

public class LoginSteps extends ScenarioSteps {

    private MyAccount login;

    @Step("Verify Login Page Title")
    public void verifyLoginPageTitle() {
        login.verifyLoginPageTitle();
    }

    @Step("Fill in credentials and click login button")
    public void authenticate(String user, String pass) {
        login.authenticate(user, pass);
    }

    @Step("Check if empty fields error is displayed")
    public String getFieldsEmptyError() {
        return login.getFieldsEmptyError();
    }

    @Step("Check if password field empty error is displayed")
    public String getPassEmptyFieldError() {
        return login.getPassEmptyFieldError();
    }

    @Step("Check if unregistered user error is displayed")
    public String getUnregisteredUserError() {
        return login.getUnregisteredUserError();
    }

    @Step("Check if dashboard text is displayed")
    public String getDashboardText() {
        return login.getDashboardText();
    }

    @Step("Logout")
    public void logout() {
        login.logout();
    }
}
