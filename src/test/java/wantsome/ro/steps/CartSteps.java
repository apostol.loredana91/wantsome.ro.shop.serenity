package wantsome.ro.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import wantsome.ro.pages.Cart;

public class CartSteps extends ScenarioSteps {

    private Cart cart;

    @Step("Update cart product quantity")
    public void updateProductQuantity(String quantity) {
        cart.updateProductQuantity(quantity);
    }

    @Step("Check if quantity was updated")
    public String getUpdateQuantityMessage() {
        return cart.getUpdateQuantityMessage();
    }

    @Step("Get card product's quantity")
    public String getCartProductQuantity() {
        return cart.getCartProductQuantity();
    }

    @Step("Go to checkout page")
    public void clickOnCheckoutButton() {
        cart.clickOnCheckoutButton();
    }
}
