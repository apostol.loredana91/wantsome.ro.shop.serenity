package wantsome.ro.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import wantsome.ro.pages.MyAccount;

public class RegistrationSteps extends ScenarioSteps {

    private MyAccount register;

    @Step("Register an account")
    public void register(String email, String pass, String fName, String lName, String phone) {
        register.registerAccount(email, pass, fName, lName, phone);
    }

    @Step("Check if empty registration email field error is displayed")
    public String getEmptyRegistrationEmailFiedError() {
        return register.getEmptyRegistrationEmailFieldError();
    }

    //-------------------------------------------------
    @Step("Define '{0}' as Email Address")
    public void setEmail(String email) {
        register.setEmail(email);
    }

    @Step("Define '{0}' as password")
    public void setPassword(String pass) {
        register.setPassword(pass);
    }

    @Step("Define '{0}' as fname first name")
    public void setFname(String fname) {
        register.setFname(fname);
    }

    @Step("Define '{0}' as lname last name")
    public void setLastName(String lname) {
        register.setFname(lname);
    }

    @Step("Define '{0}' as Phone")
    public void setPhone(String phone) {
        register.setPhone(phone);
    }

    @Step("Click register button")
    public void clickSubmit() {
        register.clickSubmit();
    }
}
