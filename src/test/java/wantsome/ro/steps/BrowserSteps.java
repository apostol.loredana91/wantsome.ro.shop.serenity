package wantsome.ro.steps;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.runner.RunWith;
import org.openqa.selenium.Point;

@RunWith(SerenityRunner.class)

public class BrowserSteps extends ScenarioSteps {

    @Step("Navigate to {0} Wantsome Shop application")
    public void goToWantsomeShopPage(String urlAddress) {
        getDriver().get(urlAddress);
        getDriver().manage().window().setPosition(new Point(0, 0));
        getDriver().manage().window().maximize();

    }

    @Step("Close the browserSteps instance")
    public void closeWebDriver() {
        getDriver().close();
        getDriver().quit();
    }

}
