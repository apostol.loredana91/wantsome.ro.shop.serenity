package wantsome.ro.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import wantsome.ro.BaseTestClass;

import java.util.concurrent.TimeUnit;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

@RunWith(SerenityRunner.class)

public class LoginTests extends BaseTestClass {
    String username = "email_2@mailinator.com";
    String password = "Strongpassword1!";
    String fieldsEmptyError = "Error: Username is required.";
    String passFieldEmptyError = "ERROR: The password field is empty.";
    String unregisteredUserError = "ERROR: Invalid username. Lost your password?";
    String expectedDashboard = "Dashboard";

    @Before
    public void ImplicitWait() {
        getDriver().manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    @Title("Login with valid credentials")
    public void validLoginVerification() {
        loginSteps.authenticate(username, password);
        Assert.assertEquals(expectedDashboard, loginSteps.getDashboardText());
        loginSteps.logout();
    }

    @Test
    @Title("Pressing login button without filling any field")
    public void loginWithEmptyFields() {
        loginSteps.authenticate("", "");
        Assert.assertEquals(fieldsEmptyError, loginSteps.getFieldsEmptyError());
        loginSteps.verifyLoginPageTitle();
    }

    @Test
    @Title("Pressing login button without filling the password field")
    public void loginWithEmptyPasswordField() {
        loginSteps.authenticate(username, "");
        Assert.assertEquals(passFieldEmptyError, loginSteps.getPassEmptyFieldError());
    }

    @Test
    @Title("Login with an unregistered user")
    public void loginWithUnregisteredUser() {
        loginSteps.authenticate("automat", "somepass");
        Assert.assertEquals(unregisteredUserError, loginSteps.getUnregisteredUserError());
    }

}
