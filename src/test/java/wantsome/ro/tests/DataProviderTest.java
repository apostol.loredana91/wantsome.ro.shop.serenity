package wantsome.ro.tests;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

@RunWith(DataProviderRunner.class)
public class DataProviderTest {
    @DataProvider
    public static Object[][] loginDataProvider() {
        return readCSVFile("./src/test/resources/LoginCredential.csv");
    }

    public static Object[][] readCSVFile(String csvFile) {
        String line = "";
        String csvSplitBy = ",";
        ArrayList<Object[]> dataOutput = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] loginData = line.split(csvSplitBy);
                dataOutput.add(loginData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Object[][] outputMatrix = new Object[dataOutput.size()][];
        for (int i = 0; i < dataOutput.size(); i++) {
            outputMatrix[i] = dataOutput.get(i);
        }
        return outputMatrix;
    }

    @Test
    @UseDataProvider("loginDataProvider")
    public void willTestSomething(String user, String password, String isValid) {
        System.out.println(user + password + isValid);
    }
}


