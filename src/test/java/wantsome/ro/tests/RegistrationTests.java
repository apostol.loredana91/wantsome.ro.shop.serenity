package wantsome.ro.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import wantsome.ro.BaseTestClass;

import java.util.concurrent.TimeUnit;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

@RunWith(SerenityRunner.class)
public class RegistrationTests extends BaseTestClass {
    String homePageTitle = "Wantsome Shop – Wantsome? Come and get some!";
    String email = "registrationuser" + RandomStringUtils.randomNumeric(3) + "@mailinator.com";
    String pass = "Strongpassword1!";
    String fName = "User";
    String lName = RandomStringUtils.randomNumeric(5);
    String phone = RandomStringUtils.randomNumeric(10);
    String expectedTitle = "Wantsome Shop – Wantsome? Come and get some!";
    String expectedEmptyRgistrationEmailFied = "Error: Please provide a valid email address.";

    @Before
    public void ImplicitWait() {
        getDriver().manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    @Title("Register an account")
    public void validRegistrationTest() {
        registrationSteps.register(email, pass, fName, lName, phone);
        Assert.assertTrue(homePageTitle.equals(homeSteps.getHomepageTitle()));
    }

    @Test
    @Title("Press register button without filling the e-mail field")
    public void registrationWithoutFillingEmailFieldTest() {
        registrationSteps.register("", pass, fName, lName, phone);
        Assert.assertEquals(expectedEmptyRgistrationEmailFied, registrationSteps.getEmptyRegistrationEmailFiedError());
    }
}
