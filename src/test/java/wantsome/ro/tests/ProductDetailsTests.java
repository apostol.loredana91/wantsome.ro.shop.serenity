package wantsome.ro.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import wantsome.ro.BaseTestClass;

@RunWith(SerenityRunner.class)

public class ProductDetailsTests extends BaseTestClass {

    String quantity = "3";
    String description = "Tast review " + RandomStringUtils.randomNumeric(2);
    String name = "User" + RandomStringUtils.randomNumeric(3);
    String email = "email" + RandomStringUtils.randomNumeric(4) + "@email.com";

    @Test
    @Title("Checking if I am able to add products to cart")
    public void addToCartTest() {
        homeSteps.navigateToMenCollection();
        productListSteps.clickOnProduct(1);
        productDetailsSteps.addProductToCart(quantity);
        String expectedConfirmationMessage = "View cart\n" + quantity + " × “" +
                productDetailsSteps.getProductTitle() + "” have been added to your cart.";
        Assert.assertEquals(expectedConfirmationMessage, productDetailsSteps.productAddedToCartConfirmationMessage());
    }

    @Test
    @Title("Add a product review")
    public void addAReviewTest() {
        homeSteps.navigateToMenCollection();
        productListSteps.clickOnProduct(1);
        productDetailsSteps.addReview(description, name, email);
        Assert.assertEquals(description, productDetailsSteps.getLastReviewDescription());
    }
}
