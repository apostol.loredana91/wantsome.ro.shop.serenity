package wantsome.ro.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import wantsome.ro.BaseTestClass;

@RunWith(SerenityRunner.class)

public class CartTests extends BaseTestClass {
    String initialQuantity = "1";
    String updatedQuantity = "3";
    String cartUpdatedMessage = "Cart updated.";
    String quantity = "2";

    @Test
    @Title("Check if cart product quantity was updated")
    public void updateCartProductQuantityTest() {
        homeSteps.navigateToMenCollection();
        productListSteps.clickOnProduct(1);
        productDetailsSteps.addProductToCart(initialQuantity);
        productDetailsSteps.navigateToCart();
        cartSteps.updateProductQuantity(updatedQuantity);
        Assert.assertEquals(cartUpdatedMessage, cartSteps.getUpdateQuantityMessage());
    }

    @Test
    @Title("Add product to cart and go to checkout")
    public void goToCheckoutTest() throws InterruptedException {
        homeSteps.navigateToMenCollection();
        productListSteps.clickOnProduct(2);
        productDetailsSteps.addProductToCart(quantity);
        productDetailsSteps.navigateToCart();
        String prodQuantity =
                cartSteps.getCartProductQuantity();
        Assert.assertEquals(quantity, prodQuantity);
        cartSteps.clickOnCheckoutButton();
        String actualPageTitle = driver.getTitle();
        Assert.assertEquals("Checkout – Wantsome Shop", actualPageTitle);
    }

}
