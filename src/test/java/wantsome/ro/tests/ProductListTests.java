package wantsome.ro.tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import wantsome.ro.BaseTestClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

@RunWith(SerenityRunner.class)

public class ProductListTests extends BaseTestClass {
    String option2 = "Sort by popularity";
    String option5 = "Sort by price: low to high";
    String expectedProduct = "Jeans";

    @Before
    public void waitForElements() {
        getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

    }

    @Test
    public void sortProductsByPopularity_MenCollectionTest() {
        homeSteps.navigateToMenCollection();
        productListSteps.sortBy(option2);
        Assert.assertEquals(expectedProduct, productListSteps.getProductTitle(0));
    }

    @Test
    public void verifyThatClickingOnProductTitleLeadsToProductPage() {
        homeSteps.navigateToMenCollection();
        productListSteps.clickOnProduct(2);
        Assert.assertEquals(expectedProduct, productDetailsSteps.getProductTitle());
    }

    @Test
    public void sortProductByLowerPriceTest_WomenCollectionTest() {
        homeSteps.navigateToWomenCollection();
        productListSteps.sortBy(option5);
        List<Double> actualPrices =
                productListSteps.getTheListOfActualProductPrices(productListSteps.getProductPrices());
        List<Double> expectedPrices =
                productListSteps.getTheListOfExpectedProductPrices();
        Assert.assertEquals(actualPrices, expectedPrices);
    }
}
