package wantsome.ro.tests;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.junit.annotations.TestData;
import org.junit.Test;
import org.junit.runner.RunWith;
import wantsome.ro.BaseTestClass;

import java.util.Arrays;
import java.util.Collection;

@WithTags({
        @WithTag(type = "priority", name = "High"),
})
@Narrative(text = {"In order to buy some products\n",
        "As a simple mortal user\n",
        "I want to be able to register within the application"}
)

@RunWith(SerenityParameterizedRunner.class)
public class RegistrationTestsWithMultipleTestData extends BaseTestClass {
    @TestData
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {"userno1@mailinator.com", "Test123!", "First1", "Last1", "5345248520"},
                {"userno2@mailinator.com", "Test123!", "First2", "Last2", "5345248521"},
                {"userno3@mailinator.com", "Test123!", "First3", "Last3", "5345248522"},
        });

    }

    private final String email;
    private final String pass;
    private final String fname;
    private final String lname;
    private final String phone;


    public RegistrationTestsWithMultipleTestData(String email, String pass, String fname, String lname, String phone) {
        this.email = email;
        this.pass = pass;
        this.fname = fname;
        this.lname = lname;
        this.phone = phone;
    }

    @Test
    @Title("Verifying that I am able to register")
    public void registerWithMultipleSteps() {
        homeSteps.navigateToConnect();
        registrationSteps.setEmail(email);
        registrationSteps.setPassword(pass);
        registrationSteps.setFname(fname);
        registrationSteps.setLastName(lname);
        registrationSteps.setPhone(phone);
        registrationSteps.clickSubmit();
    }
}
